import { DomainEvent } from "../domain/DomainEvent";
import { IPublisher } from "../domain/IPublisher";
import { ISubscriber } from "../domain/ISubscriber";


export class Publisher implements IPublisher {
    private subscribers:ISubscriber[] = [];

    publishEvent(event: DomainEvent<any>): void {
        for(const sub of this.subscribers) {
            sub.handleEvent(event);
        }
    }
    subscribe(subscriber: ISubscriber): void {
        if(!this.subscribers.includes(subscriber)) {
            this.subscribers.push(subscriber);
        }
    }

}