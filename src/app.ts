import { DomainEvent } from "./domain/DomainEvent";
import { User } from "./domain/user/User";
import { UserRegistered } from "./domain/user/user-events";



const history: DomainEvent<User>[] = [
    new UserRegistered('test', 'test@test.com', '1234')
];

const publishEvent = (event) => history.push(event);


const user = new User(history);

user.logUser('test@test.com', '123', publishEvent);
user.logUser('test@test.com', '123', publishEvent);
user.logUser('test@test.com', '123', publishEvent);
user.logUser('test@test.com', '123', publishEvent);
user.logUser('test@test.com', '123', publishEvent);

console.log(history);
