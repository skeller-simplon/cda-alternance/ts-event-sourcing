import { DomainEvent } from "../DomainEvent";

import { v4 } from 'uuid';
import { UserAccountLocked, UserChangedPassword, UserDeleted, UserLogged, UserRegistered, UserWrongLoginPassword } from "./user-events";

const maxWrongPasswordAttemps = 3;
const lockedAccoundTimeMs = 600000;

export class User {
    private uuid: string;
    private deleted = false;
    private email: string;
    private password: string;
    private wrongPasswordAttemps: number = 0;
    private lockedTimestamp: number = null;

    constructor(domainEvents: DomainEvent<User>[]) {
        for (const event of domainEvents) {
            this.apply(event);
        }
    }


    private apply(event: DomainEvent<User>) {
        if (event instanceof UserRegistered) {
            this.uuid = event.uuid;
            this.email = event.email;
            this.password = event.password;
        }
        if (event instanceof UserDeleted) {
            this.deleted = true;
        }
        if (event instanceof UserWrongLoginPassword) {
            this.wrongPasswordAttemps = event.wrongPasswordAttemps;
        }
        if (event instanceof UserAccountLocked) {
            this.lockedTimestamp = event.lockedTimestamp;
        }
        if (event instanceof UserChangedPassword) {
            this.password = event.password;
            this.lockedTimestamp = null;
        }
    }

    static registerUser(email: string, password: string, publishEvent: Function) {
        if (!validateEmail(email)) return;
        if (!validatePassword(password)) return
        const uuid = v4();
        const event = new UserRegistered(uuid, email, password);

        publishEvent(event);

        return uuid;
    }

    logUser(email: string, password: string, publishEvent: Function) {
        if (this.deleted) return;
        if (email !== this.email) return;
        if (this.wrongPasswordAttemps >= maxWrongPasswordAttemps) return;
        console.log(this.lockedTimestamp)
        console.log(new Date(this.lockedTimestamp + lockedAccoundTimeMs));

        if (new Date() < new Date(this.lockedTimestamp + lockedAccoundTimeMs)) return;
        if (password !== this.password) {
            // Ajout du nouveau compte de mauvais mot de passe;
            let newWrongPasswordAttempsCount = this.wrongPasswordAttemps + 1
            const wrongLoginEvent = new UserWrongLoginPassword(this.uuid, newWrongPasswordAttempsCount)
            this.apply(wrongLoginEvent);
            publishEvent(wrongLoginEvent);

            // Blockage du compte
            if (this.wrongPasswordAttemps === maxWrongPasswordAttemps) {
                const userAccountLockedEvent = new UserAccountLocked(this.uuid, new Date().getTime())
                this.apply(userAccountLockedEvent);
                publishEvent(userAccountLockedEvent);
            }
        } else {
            const event = new UserLogged(this.uuid)
            publishEvent(event);
        };
    }

    changePassword(newPassword: string, publishEvent: Function) {
        if (this.deleted) return;
        if (!validatePassword(newPassword)) return

        const event = new UserChangedPassword(this.uuid, newPassword)
        this.apply(event);
        publishEvent(event);
    }

    deleteUser(sourceUserUuid: string, publishEvent: Function) {
        if (this.deleted) return;
        const event = new UserDeleted(this.uuid);
        this.apply(event);
        publishEvent(event);
    }

    // login(password: string, publishEvent: Function) {
    //     if (this.deleted || this.wrongPasswordAttemps >= 3) return;

    //     if (new Date() < new Date(this.lockedTimestamp + lockedAccoundTimeMs)) return;

    //     let event: UserWrongLoginPassword | UserLogged;
    //     if (this.password !== password) {
    //         event = new UserWrongLoginPassword(this.uuid, this.wrongPasswordAttemps);
    //     } else {
    //         event = new UserLogged(this.uuid);
    //     }

    //     this.apply(event);
    //     publishEvent(event);
    // }
}


const validateEmail = (email) => {
    //Algo qui vérifie si l'email est valide
    return true;
}

const validatePassword = (pwd: string) => pwd.length < 4;