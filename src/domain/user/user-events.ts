import { DomainEvent } from "../DomainEvent";
import { User } from "./User";



export class UserRegistered extends DomainEvent<User> {
    constructor(public uuid: string, public email: string, public password: string) {
        super();
    }
}

export class UserDeleted extends DomainEvent<User> {
    constructor(public uuid: string) {
        super();
    }
}

export class UserLogged extends DomainEvent<User> {
    constructor(public uuid: string) {
        super();
    }
}

export class UserChangedPassword extends DomainEvent<User> {
    constructor(public uuid: string, public password: string) {
        super();
    }
}

export class UserWrongLoginPassword extends DomainEvent<User> {
    constructor(public uuid: string, public wrongPasswordAttemps: number) {
        super();
    }
}

export class UserAccountLocked extends DomainEvent<User> {
    constructor(public uuid: string, public lockedTimestamp: number) {
        super();
    }
}

export class UserFailedLogin extends DomainEvent<User> {
    constructor(public uuid:string){
        super();
    }
}