import { DomainEvent } from "./DomainEvent";
import { ISubscriber } from "./ISubscriber";



export interface IPublisher {
    publishEvent(event:DomainEvent<any>):void;

    subscribe(subscriber:ISubscriber):void;
}