import { DomainEvent } from "./DomainEvent";


export interface IEventStore {
    save(event:DomainEvent<any>):Promise<void>;
    find(uuid:string):Promise<DomainEvent<any>>;
}